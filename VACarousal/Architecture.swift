//
//  Architecture.swift
//  VACarousal
//
//  Created by Vikash Anand on 03/03/19.
//  Copyright © 2019 Vikash Anand. All rights reserved.
//

import UIKit

struct Architecture {
    var pictureClickedBy: String
    var image: UIImage?
    var tintColor: UIColor
    
    static func getArchitectures() -> [Architecture] {
        return ["alexander-tsang", "cj-dayrit", "cynthia-young", "elena-saharova", "kirill", "larissa", "luke-collinson", "lysander-yuen", "maximilian-keppeler", "mitya-ivanov", "nicolas-solerieu", "orlova-maria", "paul-hanaoka", "pierre-chatel-innocenti", "serge-kutuzov", "simone-hutsch", "thijs-slootjes"].map { Architecture(pictureClickedBy: $0, image: UIImage(named: $0), tintColor: UIColor(red: CGFloat.random(in: 0...1), green: CGFloat.random(in: 0...1), blue: CGFloat.random(in: 0...1), alpha: 0.2)) }
    }
}
